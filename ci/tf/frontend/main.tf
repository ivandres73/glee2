provider "aws" {
  region     = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
}

terraform {
  backend "s3" {
    #bucket is set in cli
    region = "us-east-1"
  }
}

module "frontend-sprint0" {
  source              = "AcklenAvenue/modules/aws//modules/s3"
  name                = var.name_prefix
  name_prefix         = var.name_prefix
  aws_certificate_arn = var.aws_certificate_arn
  hosted_zone_name    = var.hosted_zone_name
  zone_id             = var.zone_id
  project             = var.application
}
