variable "access_key" {}
variable "secret_key" {}
variable "aws_region" {}
variable "name_prefix" {}
variable "environment" {}
variable "application" {}

variable "aws_certificate_arn" {}

variable "hosted_zone_name" {}

variable "zone_id" {}
