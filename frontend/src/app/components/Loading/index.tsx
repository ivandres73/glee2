import React from 'react';
import ReactLoading from 'react-loading';

export declare namespace Callback {
  export interface Props {}
}

const containerStyle: React.CSSProperties = {
  width: '99vw',
  height: '95vh',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
};

export const Loading: React.FC<Callback.Props> = () => {
  return (
    <div style={containerStyle}>
      <ReactLoading width={'5%'} height={'5%'} type='spin' color='#FF9051' />
    </div>
  );
};
