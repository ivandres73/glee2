import * as React from 'react';
import style from './style.local.css';

export declare namespace LoadingMessage {
  export interface Props {
    message: string;
  }
}

export const LoadingMessage = (props: LoadingMessage.Props) => {
  return (
    <div className={style.loading}>
      <span>{props.message}</span>
    </div>
  );
};
