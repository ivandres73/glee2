import * as React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import { Breadcrumb } from './index';

export default {
  title: 'BreadCrumb',
  component: Breadcrumb,
  decorator: [withKnobs],
};

const firstMenu = [
  {
    text: text('First', 'Employees'),
    func: action('clicked'),
  },
  {
    text: text('Second', 'View & Manage'),
    func: action('clicked'),
  },
  {
    text: text('Third', 'Add New'),
  },
];

const secondMenu = [
  {
    text: 'Employees',
    func: action('clicked'),
  },
  {
    text: 'View & Manage',
    func: action('clicked'),
  },
];

export const AddNewEmployeeBreadcrumb = () => (
  <Breadcrumb
    rootPathName={text('Label', 'Add a New Employee')}
    isSecondaryPage={true}
    menu={firstMenu}
  />
);

export const ViewEmployeesBreadcrumb = () => (
  <Breadcrumb
    rootPathName='View & Manage Employees'
    isSecondaryPage={false}
    menu={secondMenu}
  />
);
