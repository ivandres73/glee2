import * as React from 'react';
import '../../style.local.css';
import style from '../../style.local.css';
import { BreadcrumbMenu, BreadCrumbMenu } from '../BreadcrumbMenu';

export declare namespace BreadCrumb {
  export interface Props {
    children?: any;
    rootPathName?: string;
    isSecondaryPage: boolean;
    menu: Array<BreadCrumbMenu.MenuItem>;
  }
}

export const Breadcrumb = (props: BreadCrumb.Props) => {
  return (
    <div className={`${style.cell} ${style['medium-6']} ${style['small-12']}`}>
      <nav aria-label='You are here:' role='navigation'>
        <BreadcrumbMenu menu={props.menu} />
      </nav>
      <h3 className={style['s-title']}>
        {props.isSecondaryPage ? (
          <a className={style['e-button-back']} href='#'>
            <i className={`${style.icon} ${style['i-chevron-left']}`} />
          </a>
        ) : (
          ''
        )}

        <span>{props.rootPathName ? props.rootPathName : ''}</span>
      </h3>
    </div>
  );
};
