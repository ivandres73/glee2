import * as React from 'react';
import style from './style.local.css';

export declare namespace Empty {
  export interface Props {
    title: string;
  }
}

export const Empty = ({ title }: Empty.Props) => {
  return (
    <div className={style['no-orders']}>
      <span className={style.icon} />
      <h3 className={style.title}>{title}</h3>
    </div>
  );
};
