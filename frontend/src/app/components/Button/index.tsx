import * as React from 'react';
import style from './style.local.css';
import classNames from 'classnames';

export declare namespace Button {
  export interface Props {
    className?: string;
    style?: React.CSSProperties;
    onClick?: any;
    onMouseLeave?: any;
    title?: string;
    disabled?: boolean;
    type?: any;
    children?: any;
  }
}

const getClassNames = (className?: string) => {
  const propClassName = className || '';
  return classNames({
    [style.button]: true,
    [propClassName]: propClassName,
  });
}

export const Button = (props: Button.Props) => {
  return (
    <button
      type={props.type || 'button'}
      style={props.style}
      className={getClassNames(props.className)}
      onClick={props.onClick}
      title={props.title}
      disabled={props.disabled}
      onMouseLeave={props.onMouseLeave}
    >
      {props.children}
    </button>
  );
}
