import * as React from 'react';
// import style from "./style.local.css";
// import { NavbarButton } from "app/components";
import { RouteComponentProps } from 'react-router';
import { withAuth0, WithAuth0Props } from '@auth0/auth0-react';
import '../../style.local.css';
import style from '../../style.local.css';
import { NavbarItem } from './components/NavbarItem';
import { NavbarSection } from './components/NavbarSection';
import { Button } from '../Button';

export interface Props
  extends WithAuth0Props,
    Pick<RouteComponentProps<void>, 'history'> {}

export interface State {
  isEmployeesActive: boolean;
  // isInventoryActive: boolean;
  currentPath: string;
}

class Navbar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      isEmployeesActive: true,
      // isInventoryActive: false,
      currentPath: props.history.location.pathname,
    };
  }

  componentDidMount() {
    this.updateCurrentMenu();
  }

  componentDidUpdate() {
    const prevPath = this.state.currentPath;
    const { pathname: currPath } = this.props.history.location;
    if (prevPath !== currPath) this.updateCurrentMenu();
  }

  updateCurrentMenu() {
    const path = this.props.history.location.pathname;
    this.setState({
      isEmployeesActive: /employees/.test(path),
      // isInventoryActive: /inventory/.test(path),
      currentPath: path,
    });
  }

  Logout = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();
    this.props.auth0.logout({ returnTo: window.location.origin });
  };

  shouldViewChange(destination: string) {
    return destination !== this.state.currentPath;
  }

  goToEmployees = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    const destination = '/employees';
    if (this.shouldViewChange(destination))
      this.props.history.push(destination);
  };

  goToAddEmployee = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    const destination = '/employees/new';
    if (this.shouldViewChange(destination))
      this.props.history.push(destination);
  };

  employeeSectionItems = [
    {
      className: 'active',
      onClick: this.goToEmployees,
      children: () => (
        <>
          <i
            className={`${style.icon} ${style['i-list']} ${style['margin-right']}`}
          />
          <span>View & Manage</span>
        </>
      ),
    },
    {
      onClick: this.goToAddEmployee,
      children: () => (
        <>
          <i
            className={`${style.icon} ${style['i-plus']} ${style['margin-right']}`}
          />
          <span>Add New</span>
        </>
      ),
    },
  ];

  render() {
    return (
      <div
        className={`${style['g-wrapper']} ${style['page-edit']} ${style['grid-container']}`}
      >
        <div className={style['g-sidebar']}>
          <Button
            className={`${style['mob-closenav']} ${style['close-button']} `}
            aria-label='Close Navigation Bar'
            type='button'
          >
            <span aria-hidden='true'>×</span>
          </Button>

          <div className={style['g-applogo']}>
            <h1 className={style.logoimage}>
              <a href='#' aria-label='Home page'>
                <span>Glee</span>
              </a>
            </h1>
          </div>

          <ul
            className={`${style.vertical} ${style.menu} ${style['accordion-menu']}`}
            data-accordion=''
          >
            <NavbarSection
              className={`${style['accordion-item']} ${style['is-active']}`}
              dataAccordionItem=''
              navbarItems={this.employeeSectionItems}
            >
              <i
                className={`${style.icon} ${style['i-user']} ${style['margin-right']}`}
              />
              <span>Employees</span>
            </NavbarSection>
            <NavbarItem onClick={this.Logout}>
              <i
                className={`${style.icon} ${style['i-logout']} ${style['margin-right']}`}
              />
              <span>Logout</span>
            </NavbarItem>
          </ul>
        </div>
      </div>
    );
  }
}

export default withAuth0<Props>(Navbar);
