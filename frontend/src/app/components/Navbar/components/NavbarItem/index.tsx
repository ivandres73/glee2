import * as React from 'react';

export declare namespace NavbarItem {
  export interface Props {
    className?: string;
    href?: string;
    onClick?: any;
    children?: any;
  }
}

export const NavbarItem = (props: NavbarItem.Props) => {
  return (
    <li className={props.className || ''}>
      <a href={props.href || '#'} onClick={props.onClick || null}>
        {props.children}
      </a>
    </li>
  );
};
