import * as React from 'react';

export declare namespace Input {
  export interface Props {
    className?: string;
    type?: any;
    placeholder?:string;
    onFocus?:any;
    onBlur?:any;
    value?:string;
    onKeyDown?:any;
    onChange?:any
    disabled?:boolean
    title?:string
    ref?:(input:any) => void
  }
}

export const Input = (props: Input.Props) => {
  return (
    <input
    className={props.className}
    type={props.type}
    placeholder={props.placeholder}
    onFocus={props.onFocus}
    onBlur={props.onBlur}
    value={props.value}
    onKeyDown={props.onKeyDown}
    onChange={props.onChange}
    disabled={props.disabled}
    title={props.title}
    ref={props.ref}
  />
  );
}
