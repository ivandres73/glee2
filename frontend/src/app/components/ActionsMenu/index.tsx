import React/*, { useState }*/ from 'react';
import '../../style.local.css';
import style from '../../style.local.css';
import { Button } from '../Button';
import { RouteComponentProps } from 'react-router';
import ContextMenu from 'axui-contextmenu';
import './style.css';
import { EmployeeActions } from 'app/containers/Employee/actions';

export declare namespace ActionsMenu {
  export interface Props extends RouteComponentProps<void> {
    employeeId: string;
    isActive: boolean;
    actions: EmployeeActions;
    match: any;
  }
}

const handleContextMenu = (e: any, menu: ContextMenu, props: ActionsMenu.Props) => {
  e.preventDefault();

  menu.setMenu([
    {
      label: 'View',
      click: () => {
        props.history.push(`/employees/${props.employeeId}/view`);
      },
    },
    {
      label: 'Edit',
      click: () => {
        props.history.push(`/employees/${props.employeeId}/edit`);
      },
    },
    {
      label: !props.isActive ? 'Activate' : 'Deactivate',
      click: () => {
        Promise.resolve(
          props.isActive
            ? props.actions.deactivateEmployee(props.employeeId)
            : props.actions.activateEmployee(props.employeeId)
        ).then(() => props.actions.fetchEmployees());
      },
    },
  ]);
  menu.popup({ x: e.pageX, y: e.pageY });
};

export const ActionsMenu = (props: ActionsMenu.Props) => {
  const menu = new ContextMenu({ id: 'basic' });

  return (
    <div onClick={(e) => handleContextMenu(e, menu, props)}>
      <Button>
        <a className={style['btn-icon']}>
          <i className={`${style.icon} ${style['i-more-1']}`} />
        </a>
      </Button>
    </div>
  );
}
