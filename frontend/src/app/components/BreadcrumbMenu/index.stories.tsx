import * as React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import { BreadcrumbMenu } from './index';

export default {
  title: 'BreadCrumbMenu',
  component: BreadcrumbMenu,
  decorator: [withKnobs],
};

const firstMenu = [
  {
    text: text('First', 'Employees'),
    func: action('clicked'),
  },
  {
    text: text('Second', 'View & Manage'),
    func: action('clicked'),
  },
  {
    text: text('Third', 'Add New'),
  },
];

const secondMenu = [
  {
    text: 'Employees',
    func: action('clicked'),
  },
  {
    text: 'View & Manage',
    func: action('clicked'),
  },
];

export const AddNewEmployeeBreadcrumbMenu = () => (
  <BreadcrumbMenu menu={firstMenu} />
);

export const ViewEmployeesBreadcrumbMenu = () => (
  <BreadcrumbMenu menu={secondMenu} />
);
