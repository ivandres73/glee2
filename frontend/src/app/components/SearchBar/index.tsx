import React, { useState } from 'react';
import '../../style.local.css';
import style from '../../style.local.css';
import { Input } from '../Input';
import { Button } from '../Button';

export declare namespace SearchBar {
  export interface Props {
    searchInput?: JSX.Element;
  }

  export interface State {
    showGoButton: boolean;
    showNavigationBar: boolean;
  }
}

const onSearchFocus = (event: React.FocusEvent<HTMLInputElement>, setShowGoButton: Function) => {
  setShowGoButton(true);
};

const onSearchBlur = (event: React.FocusEvent<HTMLInputElement>, setShowGoButton: Function) => {
  setShowGoButton(false);
};

const onOpenNavigationBar = (event: React.MouseEvent<HTMLAnchorElement>, setShowNavigationBar: Function) => {
  setShowNavigationBar(true);
};

export const SearchBar = (props: SearchBar.Props) => {
  const [showGoButton, setShowGoButton] = useState(false);
  const [, setShowNavigationBar] = useState(false);

  return (
    <div className={`${style['g-topbar']} ${style['grid-x']}`}>
      <div
        className={`${style.cell} ${style['medium-10']} ${style['small-12']}`}
      >
        <div className={style['cont-menu-tablet']}>
          <a
            className={`${style['btn-menu-tablet']} ${style.button} ${style.primary} ${style.clear}`}
            aria-label='Open Navigation Bar'
            onClick={(e: React.MouseEvent<HTMLAnchorElement>) => onOpenNavigationBar(e, setShowNavigationBar)}
          >
            <i className={`${style.icon} ${style['i-menu-1']}`} />
          </a>
        </div>
        <div className={style['cont-search']}>
          <Input
            className={style['e-searchbar']}
            type='search'
            placeholder='Search in the app'
            onFocus={(e: React.FocusEvent<HTMLInputElement>) => onSearchFocus(e, setShowGoButton)}
            onBlur={(e: React.FocusEvent<HTMLInputElement>) => onSearchBlur(e, setShowGoButton)}
          />

          <Button
            className={`${style['e-searchbtn']} ${style.button} ${
              style.primary
            } ${showGoButton ? style.show : style.hide}`}
          >
            <span>Go!</span>
          </Button>
        </div>
      </div>
      <div
        className={`${style.cell} ${style['medium-2']} ${style['small-12']} ${style['text-right']}`}
      />
    </div>
  );
}
