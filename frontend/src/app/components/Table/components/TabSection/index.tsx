import * as React from 'react';
import { TableTab } from '../TableTab';

export declare namespace TabSection {
  export interface Props {
    className?: string;
    tabs: Array<TabSection.Tab>;
  }

  export interface Tab {
    title: any;
    onClick: any;
    className?: string;
  }
}

const renderTabs = (tabs: Array<TabSection.Tab>) => {
  return tabs.map((tab, index) => (
    <TableTab className={tab.className} onClick={tab.onClick} key={index}>
      {tab.title}
    </TableTab>
  ));
};

export const TabSection = (props: TabSection.Props) => {
  return (
    <ul className={props.className || ''} data-tabs='emp-list-control'>
      {renderTabs(props.tabs)}
    </ul>
  );
};
