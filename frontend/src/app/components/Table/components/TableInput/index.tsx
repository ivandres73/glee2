import * as React from 'react';
import appstyle from '../../../../style.local.css';
import { Button } from 'app/components/Button';

export declare namespace TableInput {
  export interface Props {
    inputValue: any;
    onChange: any;
    placeholder?: string;
    onClickSearchButton: any;
  }
}

export const TableInput = (props: TableInput.Props) => {
  return (
    <div className={`${appstyle['cont-table-filters']}`}>
      <div className={`${appstyle['input-group']}`} style={{ marginBottom: 0 }}>
        <input
          type='text'
          placeholder={props.placeholder || 'Filter by Display Name...'}
          value={props.inputValue}
          onChange={props.onChange}
        />
        <div className={`${appstyle['input-group-button']}`}>
          <Button 
            className={`${appstyle.hollow}`}
            onClick={props.onClickSearchButton}
          >
            <i
              className={`${appstyle.icon} ${appstyle['i-search']}`}
            />
          </Button>
        </div>
      </div>
    </div>
  );
};
