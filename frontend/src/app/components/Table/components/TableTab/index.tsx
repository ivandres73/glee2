import * as React from 'react';

export declare namespace TableTab {
  export interface Props {
    onClick: any;
    className?: string;
    children: any;
  }
}

export const TableTab = (props: TableTab.Props) => {
  return (
    <li className={props.className || ''}>
      <a href='#' onClick={props.onClick}>
        {props.children}
      </a>
    </li>
  );
};
