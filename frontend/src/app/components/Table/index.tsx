import React, { useState } from 'react';
import classNames from 'classnames';
import ReactTable, { Column, SortingRule } from 'react-table';

import appstyle from '../../style.local.css';
import style from './react-table.css';
import '../../style.local.css';
import './react-table.css';
import { TabSection } from './components/TabSection';
import { TableInput } from './components/TableInput';

export declare namespace Table {
  export interface Props {
    data: ReadonlyArray<any>;
    employeeCount: number;
    loading: boolean;
    columns: Column[];
    headerStyle?: React.CSSProperties;
    style?: React.CSSProperties;
    defaultSorted: SortingRule[];
    onFilter(searchText: string, currentPage?: number, pageSize?: number): any;
    onToggleArchive(showActive: boolean): any;
  }

  export interface State {
    typingTimeout: NodeJS.Timer | number;
    searchText: string;
    showActiveEmployees: boolean;
    pageSize: number;
    currentPage: number;
  }
}

const tableClasses = () => {
  return classNames({
    [style.ReactTable]: true,
    'table-large': true,
    '-highlight': true,
    '-striped': true,
  });
}

const getHeadersStyle = (props: Table.Props) => ({ style: { ...(props.headerStyle || {}) } });

const textStyle: React.CSSProperties = {
  fontFamily: 'Mukta, Helvetica, Roboto, Arial, sans-serif',
  color: '#4f4f4f',
};

const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>, state: Table.State, setState: Function, props: Table.Props) => {
  event.preventDefault();
  if (state.typingTimeout) {
    clearTimeout(state.typingTimeout as NodeJS.Timer);
  }
  setState({
    ...state,
    searchText: event.target.value,
    typingTimeout: setTimeout((text: string) => {
      const searchText = text;
      const shouldResetSearch = searchText.length === 0;
      if (searchText.length >= 2 || shouldResetSearch) {
        props.onFilter(text);
      }
    }, 500, event.target.value),
  });
}

const toggleTab = (e: React.ChangeEvent<HTMLInputElement>, state: Table.State, setState: Function, props: Table.Props) => {
  e.preventDefault();
  if (state.showActiveEmployees) return;
  setState({
    ...state,
    showActiveEmployees: true,
  });
  props.onToggleArchive(state.showActiveEmployees);
};

const toggleArchiveTab = (e: React.ChangeEvent<HTMLInputElement>, state: Table.State, setState: Function, props: Table.Props) => {
  e.preventDefault();
  if (!state.showActiveEmployees) return;
  setState({
    ...state,
    showActiveEmployees: false,
  });
  props.onToggleArchive(state.showActiveEmployees);
};

const searchTable = (e: React.ChangeEvent<HTMLInputElement>, state: Table.State, setState: Function, props: Table.Props) => {
  e.preventDefault();
  props.onFilter(state.searchText);
}

const onPagination = (state: Table.State, setState: Function, props: Table.Props, tableState: any) => {
  setState({
    ...state,
    currentPage: tableState.page + 1,
    pageSize: tableState.pageSize
  });
  props.onFilter(state.searchText, tableState.page+1, tableState.pageSize);
}

export const Table = (props: Table.Props) => {

  const [state, setState] = useState({
    typingTimeout: 0,
    searchText: '',
    showActiveEmployees: true,
    pageSize: 10,
    currentPage: 1
  });

  const functionWrapper = (functionToCall: Function) => (e: any) => functionToCall(e, state, setState, props);

  const { data, columns, loading, defaultSorted, employeeCount } = props;
  const { pageSize } = state;
  const tabs = [
    {
      className: `${appstyle['tabs-title']} ${
        appstyle[state.showActiveEmployees ? 'is-active' : '']
      }`,
      onClick: functionWrapper(toggleTab),
      title: 'Active'
    },
    {
      className: `${appstyle['tabs-title']} ${
        appstyle[state.showActiveEmployees ? '' : 'is-active']
      }`,
      onClick: functionWrapper(toggleArchiveTab),
      title: 'Archived'
    }
  ];
  return ( 
    <div
      className={`
      ${appstyle['g-content-fluid']} ${appstyle['table-container']} ${appstyle['grid-container']} ${appstyle.fluid} ${appstyle['grid-padding-x']} ${appstyle.full}
      `}
      style={textStyle}
    >
      <form>
        <div
          className={`
          ${appstyle['grid-x']}
          ${appstyle['grid-padding-x']}
          ${appstyle['table-controls']}`}
        >
          <div
            className={`${appstyle.cell} ${appstyle['medium-12']} ${appstyle['small-12']} ${appstyle['large-8']}`}
          >
            <TabSection 
              className={`${appstyle.tabs}`}
              tabs={tabs}
            />
            <TableInput 
              inputValue={state.searchText}
              onChange={functionWrapper(handleInputChange)}
              onClickSearchButton={functionWrapper(searchTable)}
            />
          </div>
          <div
            className={`${appstyle.cell} ${appstyle['medium-12']} ${appstyle['small-12']} ${appstyle['large-4']} ${appstyle['text-right']}`}
          />
        </div>
        <div className={`${appstyle['g-main']}`}>
          <ReactTable
            data={data as any[]}
            pageSize={pageSize}
            pages={Math.ceil(employeeCount/pageSize)}
            className={`${tableClasses()} ${appstyle.hover} ${
              appstyle['table-large']
            }`}
            getTheadThProps={() => ({
              ...getHeadersStyle(props),
            })}
            columns={columns}
            noDataText=''
            loading={loading}
            defaultSorted={defaultSorted}
            multiSort={true}
            manual
            onFetchData={(tableState) => {
              onPagination(state, setState, props, tableState)
            }}
          />
        </div>
      </form>
    </div>
  );
}
