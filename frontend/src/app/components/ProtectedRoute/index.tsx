import React from 'react';
import { Route, RouteProps } from 'react-router-dom';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import { Loading } from '../Loading';

const ProtectedRoute: React.FC<RouteProps> = ({ component, ...props }) => (
  <Route
    component={
      component &&
      withAuthenticationRequired(component, {
        returnTo: window.location.pathname,
        onRedirecting: () => <Loading />,
        loginOptions: {
          appState: { returnTo: window.location.pathname },
          redirect_uri: window.location.pathname,
        },
      })
    }
    {...props}
  />
);

export default ProtectedRoute;
