import { Store, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'connected-react-router';
import { logger } from 'app/middleware';
import { RootState, rootReducer } from 'app/reducers';
import thunk from 'redux-thunk';
import history from '../../browserHistory';

export { history };

export function configureStore(initialState?: RootState): Store<RootState> {
  let middleware = applyMiddleware(logger, thunk, routerMiddleware(history));

  if (process.env.NODE_ENV !== 'production') {
    middleware = composeWithDevTools(middleware);
  }

  const store = createStore(
    rootReducer(history),
    initialState,
    middleware
  ) as Store<RootState>;

  if (module.hot) {
    module.hot.accept('app/reducers', () => {
      store.replaceReducer(rootReducer(history));
    });
  }

  return store;
}
