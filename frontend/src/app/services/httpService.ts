import { Auth0ContextInterface } from '@auth0/auth0-react';

export declare namespace HttpService {
  export interface Options {
    params: any;
  }

  export interface Pagination {
    page?: number;
    pageSize?: number;
  }
}

export class HttpService {
  constructor(private authService: Auth0ContextInterface) {}

  public async get(url: string, options?: HttpService.Options) {
    let newUrl = url;

    if (options && options.params) {
      const queryParams = this.getQueryStringFromParams(options.params);
      newUrl = newUrl + queryParams;
    }
    const token = await this.getToken();
    const response = await fetch(newUrl, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    switch (response.status) {
      case 500:
      case 404:
      case 400:
        throw response;
    }
    return response.json();
  }

  public async post(url: string, data: any) {
    const token = await this.getToken();
    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(data),
      method: 'POST',
    });
    switch (response.status) {
      case 500:
      case 404:
      case 400:
        return response.json().then((json) => Promise.reject(json));
    }
    return response;
  }

  public async put(url: string, data: any) {
    const token = await this.getToken();
    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(data),
      method: 'PUT',
    });
    switch (response.status) {
      case 500:
      case 404:
      case 400:
        return response.json().then((json) => Promise.reject(json));
    }
    return response;
  }

  public async delete(url: string) {
    const token = await this.getToken();
    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json; charset=utf-8',
      },
      // body: JSON.stringify(data),
      method: 'DELETE',
    });
    switch (response.status) {
      case 500:
      case 404:
      case 400:
        throw response;
    }
    return response;
  }

  private async getToken(): Promise<string> {
    try {
      return this.authService.getAccessTokenSilently();
    } catch (error) {
      this.authService.loginWithRedirect();
      return '';
    }
  }

  private getQueryStringFromParams(params: any): string {
    let queryString = '?';
    queryString += Object.keys(params)
      .map((key: string) => {
        const value = params[key];
        if (!value || value === 'undefined' || typeof value === 'object') {
          return '';
        }
        return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
      })
      .filter((val) => val)
      .join('&');
    return queryString;
  }
}
