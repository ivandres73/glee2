import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Auth0Provider } from '@auth0/auth0-react';
import { AppState } from '@auth0/auth0-react/dist/auth0-provider';
import { ConnectedRouter } from 'connected-react-router';
import { configureStore, history } from 'app/store';
import Root from './app';

// prepare store
const store = configureStore();

const onRedirectCallback = (appState: AppState) => {
  // Use the router's history module to replace the url
  history.replace((appState && appState.returnTo) || window.location.pathname);
};

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Auth0Provider
        domain={process.env.AUTH0_DOMAIN!}
        clientId={process.env.AUTH0_CLIENT_ID!}
        audience={process.env.AUTH0_AUDIENCE}
        redirectUri={window.location.origin}
        onRedirectCallback={onRedirectCallback}
      >
        <Root />
      </Auth0Provider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
